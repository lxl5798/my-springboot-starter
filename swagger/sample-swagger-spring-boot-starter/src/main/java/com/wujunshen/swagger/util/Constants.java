package com.wujunshen.swagger.util;

import lombok.NoArgsConstructor;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@NoArgsConstructor
public class Constants {
    public static final String NULL_DATA = "";
}
