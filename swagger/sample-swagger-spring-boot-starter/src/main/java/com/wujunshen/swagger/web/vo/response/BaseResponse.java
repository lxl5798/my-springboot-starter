package com.wujunshen.swagger.web.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@ApiModel(value = "响应消息基础类")
public class BaseResponse<T> {
    @ApiModelProperty(value = "响应码", required = true)
    private int code;
    
    @ApiModelProperty(value = "响应消息", required = true)
    private String message;
    
    @ApiModelProperty(value = "返回数据")
    private T data;
}
