package com.wujunshen.swagger;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.wujunshen.swagger.dao")
public class SampleSwaggerSpringBootStarterApplication {
    public static void main(String[] args) {
        log.info("\nstart execute SampleSwaggerSpringBootStarterApplication....\n");
        SpringApplication.run(SampleSwaggerSpringBootStarterApplication.class, args);
        log.info("\nend execute SampleSwaggerSpringBootStarterApplication....\n");
    }
}
