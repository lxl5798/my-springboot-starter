package com.wujunshen.swagger.exception;

import com.wujunshen.swagger.util.Constants;
import com.wujunshen.swagger.web.vo.response.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 * 响应对象处理器
 */
@Slf4j
@Component
public class ResponseHandler {
    public BaseResponse<Object> getBaseResponse(ResponseStatus responseStatus) {
        BaseResponse<Object> baseResponse = new BaseResponse<>();
        if (responseStatus != null) {
            baseResponse.setMessage(responseStatus.getMessage());
            baseResponse.setCode(responseStatus.getCode());
            baseResponse.setData(Constants.NULL_DATA);
        }
        
        return baseResponse;
    }
}
