package com.wujunshen.swagger.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/1/9 4:22 下午 <br>
 */
@Data
@ConfigurationProperties("swagger")
public class SwaggerProperties {
    /**
     * 开启
     */
    private boolean enable;
    
    /**
     * 调用上下文路径
     */
    private String contextPath;
    
    /**
     * 标题
     */
    private String title = "API文档";
    
    /**
     * 说明
     */
    private String description = "文档内容仅供参考";
    
    /**
     * 版本
     */
    private String[] versions = {"v1.0.0"};
    
    /**
     * license
     */
    private String license = "Apache License Version 2.0";
    
    /**
     * license链接
     */
    private String licenseUrl = "http://www.apache.org/licenses/LICENSE-2.0.html";
    
    /**
     * 联系文字
     */
    private String contactName = "黑暗浪子";
    
    /**
     * 联系人博客或个人网址
     */
    private String contactUrl = "https://www.iteye.com/blog/user/darkranger";
    
    /**
     * 联系人邮箱地址
     */
    private String contactEmail = "frank_wjs@hotmail.com";
}
