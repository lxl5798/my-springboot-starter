# prometheus

#### 项目介绍
参见prometheus-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定所需访问的两个类

PrometheusAutoConfiguration是自动装配类。
无需在自己应用里使用普罗米修斯的annotation注解就可查看其EndPoint信息

PrometheusSpringApplicationRunListener是监听器
若有需要，可在application.yml里设定management.server.port端口，走自定义的端口

若无需要，则无需配置management.server.port端口，统一使用8081端口

#### 使用说明
参见sample-prometheus-spring-boot-starter项目
1. 在pom文件中加入starter依赖

    ```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>prometheus-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    ```

2. 启动sample-prometheus-spring-boot-starter应用后，运行[http://localhost:8007/hello](http://localhost:8007/hello)

查看sample-prometheus-spring-boot-starter应用是否能正常工作

3. 运行[http://localhost:8081/actuator/prometheus](http://localhost:8081/actuator/prometheus)和[http://localhost:8081/actuator](http://localhost:8081/actuator)

查看监控端点是否能正常显示信息

4. 假设在application.yml里设定

    ```
    management:
      server:
        port: 9081
    ```

则启动sample-prometheus-spring-boot-starter应用后，运行[http://localhost:9081/actuator/prometheus](http://localhost:9081/actuator/prometheus)和[http://localhost:9081/actuator](http://localhost:9081/actuator)

查看监控端点是否能正常显示信息

如下列这些图

http://localhost:9081/actuator

![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/180609_8ce00582_43183.png "屏幕截图.png")

http://localhost:9081/actuator/prometheus

![输入图片说明](https://images.gitee.com/uploads/images/2020/0323/180654_1ff92bbb_43183.png "屏幕截图.png")

#### grafana导入jvm监控页面(grafana安装见document目录下的prometheus+grafana安装(Mac系统).pdf)

登录grafana,导入对应的JVM监控Dashboard模板，模板编号为4701,见下图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/002351_daca3c15_43183.png "屏幕截图.png")

导入完毕可看到JVM(Micrometer)各种类型指标监控图形化页面

![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/002447_44cebca0_43183.jpeg "20200324001127.jpg")

#### Java应用自定义监控指标示例

sample-prometheus-spring-boot-starter应用中,新建一个CounterController类

记录接口[http://localhost:8007/counter](http://localhost:8007/counter)被调用次数

1. 启动sample-prometheus-spring-boot-starter应用,浏览器输入[http://localhost:8007/counter](http://localhost:8007/counter)

   确保接口可以正常访问,重复刷新浏览器多次,见下图结果

    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/001535_465ba0c1_43183.jpeg "20200323225908.jpg")
   

2. 在Prometheus UI界面上使用PromSQL查询自定义的监控信息。添加Graph并执行如下查询语句

app_requests_method_count_total{application="sample-prometheus", instance="sample-prometheus", method="CounterController.counter"}

查询结果如下

![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/001619_10712925_43183.jpeg "20200323225838.jpg")

解释一下查询语句

app_requests_method_count_total为代码中设置的Counter名,见CounterController类中代码

        @PostConstruct
        private void init() {
            counter = registry.counter("app_requests_method_count", "method", "CounterController.counter");
        }


application为初始化registry时设置的通用标签，标注应用名称，这样做好处就是可以根据应用名称区分不同的应用。

instance为application.json中配置的instance实例名，用来区分应用实例

在我个人电脑中,application.json路径为/usr/local/prometheus/groups (为啥在这个路径下,具体原因也可见document目录下的prometheus+grafana安装(Mac系统).pdf)

application.json内容如下


    [
        {
            "targets": [
                "localhost:8081"
            ],
            "labels": {
                "instance": "sample-prometheus",
                "service": "sample-prometheus-service"
            }
        }
    ]


method为CounterController类中代码设置的Counter标签名，用来区分不同的方法，这样就不用为每个方法设置一个Counter了

在grafana dashboard上新建Panel并添加Query查询

添加一个Panel并命名为"自定义指标"

然后点击"Add Query"增加一个新的Query查询

查询语句为上面的app_requests_method_count_total{application="sample-prometheus", instance="sample-prometheus", method="CounterController.counter"}

为了更好的扩展性,可将application和instance两个参数赋值为变量,这些变量可以直接从prometheus上传过来.

最终的查询语句为 

app_requests_method_count_total{application="$application", instance="$instance", method="CounterController.counter"}

最后修改标题为"实时访问量/counter"，保存

返回首页就可看到刚添加的Dashboard

![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/003202_e80623f9_43183.png "屏幕截图.png")


也可点击"实时访问量/counter"标题,进行编辑,将图形变为直方图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/005301_26532661_43183.png "屏幕截图.png")

如上红框显示,点击左上角返回箭头

效果如下

![输入图片说明](https://images.gitee.com/uploads/images/2020/0324/005351_f8f59495_43183.png "屏幕截图.png")


