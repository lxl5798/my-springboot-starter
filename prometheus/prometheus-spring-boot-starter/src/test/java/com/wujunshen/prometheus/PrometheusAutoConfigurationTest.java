package com.wujunshen.prometheus;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(
        classes = TestConfiguration.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.application.name=test-prometheus"})
public class PrometheusAutoConfigurationTest {
    @Value("${management.server.port}")
    private int managementPort;
    
    @Value("${spring.application.name}")
    private String applicationName;
    
    @Test
    public void prometheusAutoConfigurationWhenActiveThenRegistersActuatorEndpoint() {
        // Given
        RestTemplate restTemplate = new RestTemplate();
        
        log.info("managementPort is:{}", managementPort);
        log.info("applicationName is:{}", applicationName);
        
        // When
        ResponseEntity<String> entity =
                restTemplate.getForEntity(
                        "http://localhost:{port}/actuator/prometheus", String.class, managementPort);
        
        // Then
        log.info("code is:{}", entity.getStatusCodeValue());
        log.info("body is:{}", entity.getBody());
        assertThat(entity.getStatusCodeValue(), is(200));
        assertThat(entity.getBody(), containsString(applicationName));
    }
}
