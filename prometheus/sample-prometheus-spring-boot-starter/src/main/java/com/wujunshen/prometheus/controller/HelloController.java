package com.wujunshen.prometheus.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@RestController
public class HelloController {
    
    @GetMapping(
            value = "/hello",
            produces = {MediaType.TEXT_PLAIN_VALUE})
    public String hello() {
        return "hello";
    }
}
