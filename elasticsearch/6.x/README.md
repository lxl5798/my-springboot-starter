# elasticsearch6.x版本

由于目前es官方6.x和7.x两个版本同时在维护，所以我们也同时提供两个版本的starter

#### 项目介绍
参见elasticsearch6-spring-boot-starter项目

在/resources/META_INF/spring.factories文件中指定自动装配类

Elasticsearch6AutoConfiguration是自动装配类

由于ES的发展规划中,从7.0版本开始将废弃TransportClient,8.0版本中将完全移除TransportClient

所以我废弃了使用TransportClient的代码,改用spring-boot-starter-data-elasticsearch依赖

因此Elasticsearch6AutoConfiguration中无任何其他配置类引用

#### 使用说明
具体可见elasticsearch6-spring-boot-starter项目中单元测试代码

1. 如果要声明依赖引入,只需在自己应用的pom文件中加入starter依赖
        
    ```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>elasticsearch6-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    ```

2. 在application.yml配置文件加入下列内容

    ```
    spring:
      data:
        #elasticsearch config
        elasticsearch:
          repositories:
            enabled: true
          cluster-nodes: localhost:9300,localhost:9301,localhost:9302
          cluster-name: elasticsearch-test
    ```

3. 运行com.wujunshen.elasticsearch6.test包

    如下图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0326/010417_b36ea957_43183.png "屏幕截图.png")