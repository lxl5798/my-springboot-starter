package com.wujunshen.elasticsearch6.test;

import static com.wujunshen.elasticsearch6.common.utils.Constants.TEST_INDEX;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wujunshen.elasticsearch6.TestConfiguration;
import com.wujunshen.elasticsearch6.common.domain.usergroup.User;
import com.wujunshen.elasticsearch6.common.domain.usergroup.UserGroup;
import com.wujunshen.elasticsearch6.common.repository.usergroup.UserGroupRepository;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.join.ScoreMode;
import org.assertj.core.util.Lists;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class UserGroupQueryTest {
    private ObjectMapper mapper = new ObjectMapper();
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;
    @Resource
    private UserGroupRepository userGroupRepository;
    
    @BeforeEach
    public void init() {
        // 创建index
        if (!elasticsearchTemplate.indexExists(TEST_INDEX)) {
            elasticsearchTemplate.createIndex(TEST_INDEX);
        }
        
        elasticsearchTemplate.putMapping(UserGroup.class);
        
        elasticsearchTemplate.refresh(UserGroup.class);
        
        elasticsearchTemplate.putMapping(User.class);
        
        elasticsearchTemplate.refresh(User.class);
    }
    
    @AfterEach
    public void clear() {
    }
    
    /**
     * 嵌套查询, 内嵌文档查询
     */
    @Test
    public void testNestedQuery() throws IOException {
        // 创建数据
        UserGroup userGroup =
                UserGroup.builder()
                        .id(1L)
                        .group("fans")
                        .user(User.builder().id(1L).first("junshen").last("wu").build())
                        .build();
        
        userGroupRepository.save(userGroup);
        
        // 准备查询
        QueryBuilder queryBuilder =
                QueryBuilders.nestedQuery(
                        "user",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("user.first", "junshen"))
                                .must(QueryBuilders.matchQuery("user.last", "wu")),
                        ScoreMode.None);
        
        Iterable<UserGroup> iterable = userGroupRepository.search(queryBuilder);
        
        List<UserGroup> list = Lists.newArrayList(iterable);
        
        log.debug(
                "\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        
        assertThat(list.size(), is(1));
        
        userGroupRepository.delete(userGroup);
    }
}
