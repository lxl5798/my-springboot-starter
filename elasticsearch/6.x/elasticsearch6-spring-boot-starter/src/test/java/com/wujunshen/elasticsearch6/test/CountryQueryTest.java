package com.wujunshen.elasticsearch6.test;

import static com.wujunshen.elasticsearch6.common.utils.Constants.TEST_INDEX;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wujunshen.elasticsearch6.TestConfiguration;
import com.wujunshen.elasticsearch6.common.domain.area.City;
import com.wujunshen.elasticsearch6.common.domain.area.Country;
import com.wujunshen.elasticsearch6.common.domain.area.District;
import com.wujunshen.elasticsearch6.common.domain.area.Province;
import com.wujunshen.elasticsearch6.common.domain.area.Region;
import com.wujunshen.elasticsearch6.common.repository.area.CountryRepository;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.join.ScoreMode;
import org.assertj.core.util.Lists;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class CountryQueryTest {
    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private CountryRepository countryRepository;
    
    @BeforeEach
    public void init() {
        // 创建index
        if (!elasticsearchTemplate.indexExists(TEST_INDEX)) {
            elasticsearchTemplate.createIndex(TEST_INDEX);
        }
        
        elasticsearchTemplate.putMapping(Country.class);
        
        elasticsearchTemplate.refresh(Country.class);
        
        elasticsearchTemplate.putMapping(Region.class);
        
        elasticsearchTemplate.refresh(Region.class);
        
        elasticsearchTemplate.putMapping(Province.class);
        
        elasticsearchTemplate.refresh(Province.class);
        
        elasticsearchTemplate.putMapping(City.class);
        
        elasticsearchTemplate.refresh(City.class);
        
        elasticsearchTemplate.putMapping(District.class);
        
        elasticsearchTemplate.refresh(District.class);
    }
    
    @AfterEach
    public void clear() {
    }
    
    /**
     * 嵌套查询, 内嵌文档查询
     */
    @Test
    public void testNestedQuery() throws IOException {
        // 创建数据
        Country country =
                Country.builder()
                        .id(1L)
                        .region(
                                Region.builder()
                                        .id(1L)
                                        .province(
                                                Province.builder()
                                                        .id(1L)
                                                        .city(
                                                                City.builder()
                                                                        .id(1L)
                                                                        .district(
                                                                                District.builder().id(1L).code(8).name("test").build())
                                                                        .build())
                                                        .build())
                                        .build())
                        .build();
        countryRepository.save(country);
        
        // 准备查询
        QueryBuilder queryBuilder =
                QueryBuilders.nestedQuery(
                        "region.province.city.district",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("region.province.city.district.code", 8))
                                .must(QueryBuilders.matchQuery("region.province.city.district.name", "test")),
                        ScoreMode.None);
        
        Iterable<Country> iterable = countryRepository.search(queryBuilder);
        
        List<Country> list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(1));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "province",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("province.city.district.code", 8))
                                .must(QueryBuilders.matchQuery("province.city.district.name", "test")),
                        ScoreMode.None);
        
        iterable = countryRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "city",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("city.district.code", 8))
                                .must(QueryBuilders.matchQuery("city.district.name", "test")),
                        ScoreMode.None);
        
        iterable = countryRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "district",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("district.code", 8))
                                .must(QueryBuilders.matchQuery("district.name", "test")),
                        ScoreMode.None);
        
        iterable = countryRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        countryRepository.delete(country);
    }
}
