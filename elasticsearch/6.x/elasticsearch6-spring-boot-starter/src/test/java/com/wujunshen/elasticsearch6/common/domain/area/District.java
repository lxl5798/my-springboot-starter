package com.wujunshen.elasticsearch6.common.domain.area;

import static com.wujunshen.elasticsearch6.common.utils.Constants.TEST_INDEX;
import static com.wujunshen.elasticsearch6.common.utils.Constants.TEST_TYPE;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(
        indexName = TEST_INDEX,
        type = TEST_TYPE,
        shards = 1,
        replicas = 0,
        refreshInterval = "-1")
@Builder
public class District {
    @Id
    private Long id;
    
    private String name;
    private int code;
}
