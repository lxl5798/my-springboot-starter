package com.wujunshen.elasticsearch7.test;

import static com.wujunshen.elasticsearch7.common.utils.Constants.ES_INDEX_TEST;
import static com.wujunshen.elasticsearch7.common.utils.Constants.ES_TYPE_TEST;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.wujunshen.elasticsearch7.TestConfiguration;
import com.wujunshen.elasticsearch7.common.domain.product.Sku;
import com.wujunshen.elasticsearch7.common.domain.product.Spu;
import com.wujunshen.elasticsearch7.common.repository.product.SpuRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class ESCreateTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;
    @Resource
    private SpuRepository spuRepository;
    private Map<String, Object> mapping;
    private Spu spu1;
    private Spu spu2;
    
    @BeforeEach
    public void init() {
        spu1 = new Spu();
        spu1.setId(1L);
        spu1.setProductCode("7b28c293-4d06-4893-aad7-e4b6ed72c260");
        spu1.setProductName("android手机");
        spu1.setBrandCode("H-001");
        spu1.setBrandName("华为Nexus");
        spu1.setCategoryCode("C-001");
        spu1.setCategoryName("手机");
        
        Sku sku1 = new Sku();
        sku1.setId(1L);
        sku1.setSkuCode("001");
        sku1.setSkuName("华为Nexus P6");
        sku1.setSkuPrice(4000);
        sku1.setColor("Red");
        
        Sku sku2 = new Sku();
        sku2.setId(2L);
        sku2.setSkuCode("002");
        sku2.setSkuName("华为 P8");
        sku2.setSkuPrice(3000);
        sku2.setColor("Blank");
        
        Sku sku3 = new Sku();
        sku3.setId(3L);
        sku3.setSkuCode("003");
        sku3.setSkuName("华为Nexus P6下一代");
        sku3.setSkuPrice(5000);
        sku3.setColor("White");
        
        spu1.getSkus().add(sku1);
        spu1.getSkus().add(sku2);
        spu1.getSkus().add(sku3);
        
        spu2 = new Spu();
        spu2.setId(2L);
        spu2.setProductCode("AVYmdpQ_cnzgjoSZ6ent");
        spu2.setProductName("运动服装");
        spu2.setBrandCode("YD-001");
        spu2.setBrandName("李宁");
        spu2.setCategoryCode("YDC-001");
        spu2.setCategoryName("服装");
        
        Sku sku21 = new Sku(21L, "YD001", "李宁衣服1", "Green", "2XL", 4000);
        Sku sku22 = new Sku(22L, "YD002", "李宁衣服2", "Green", "L", 3000);
        Sku sku23 = new Sku(23L, "YD003", "李宁衣服3", "Green", "M", 5000);
        
        spu2.getSkus().add(sku21);
        spu2.getSkus().add(sku22);
        spu2.getSkus().add(sku23);
        
        mapping = Maps.newConcurrentMap();
    }
    
    @AfterEach
    public void clear() {
        spu1 = null;
        spu2 = null;
        
        mapping = null;
    }
    
    @Test
    public void createIndex() {
        assertThat(elasticsearchTemplate.indexExists(ES_INDEX_TEST), is(false));
        
        if (!elasticsearchTemplate.indexExists(ES_INDEX_TEST)) {
            assertThat(elasticsearchTemplate.createIndex(ES_INDEX_TEST), is(true));
        }
        
        assertThat(elasticsearchTemplate.indexExists(ES_INDEX_TEST), is(true));
    }
    
    @Test
    public void deleteIndex() {
        if (elasticsearchTemplate.indexExists(ES_INDEX_TEST)) {
            assertThat(elasticsearchTemplate.deleteIndex(ES_INDEX_TEST), is(true));
        }
        
        assertThat(elasticsearchTemplate.indexExists(ES_INDEX_TEST), is(false));
    }
    
    @Test
    public void createMapping() {
        assertThat(elasticsearchTemplate.putMapping(Spu.class), is(true));
        
        elasticsearchTemplate.refresh(Spu.class);
    }
    
    @Test
    public void getMapping() throws JsonProcessingException {
        spuRepository.deleteAll();
        
        mapping = elasticsearchTemplate.getMapping(Spu.class);
        log.info("\nmapping is:{}\n", mapping);
        
        String jsonString = OBJECT_MAPPER.writeValueAsString(mapping);
        
        assertThat(jsonString, containsString("categoryCode"));
    }
    
    @Test
    public void getAllMapping() throws JsonProcessingException {
        mapping = elasticsearchTemplate.getMapping(ES_INDEX_TEST, ES_TYPE_TEST);
        log.info("\nmapping is:{}\n", mapping);
        
        String jsonString = OBJECT_MAPPER.writeValueAsString(mapping);
        
        assertThat(jsonString, containsString("categoryCode"));
    }
    
    @Test
    public void operateData() {
        spuRepository.save(spu1);
        assertThat(spuRepository.count(), is(1L));
        
        spuRepository.delete(spu1);
        assertThat(spuRepository.count(), is(0L));
    }
    
    @Test
    public void operateBatchData() {
        List<Spu> spuList = new ArrayList<>();
        spuList.add(spu1);
        spuList.add(spu2);
        
        spuRepository.saveAll(spuList);
        assertThat(spuRepository.count(), is(2L));
        
        spuRepository.deleteAll();
        assertThat(spuRepository.count(), is(0L));
    }
}
