package com.wujunshen.elasticsearch7.common.repository.product;

import com.wujunshen.elasticsearch7.common.domain.product.Sku;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/3/15 1:39 上午 <br>
 */
@Repository
public interface SkuRepository extends ElasticsearchRepository<Sku, Long> {
}
