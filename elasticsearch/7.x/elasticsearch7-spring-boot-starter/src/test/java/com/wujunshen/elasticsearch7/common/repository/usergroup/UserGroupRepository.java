package com.wujunshen.elasticsearch7.common.repository.usergroup;

import com.wujunshen.elasticsearch7.common.domain.usergroup.UserGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author frank woo(吴峻申) <br> email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 **/
@Repository
public interface UserGroupRepository
        extends ElasticsearchRepository<UserGroup, Long> {
}
