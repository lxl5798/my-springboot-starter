package com.wujunshen.elasticsearch7.test;

import static com.wujunshen.elasticsearch7.common.utils.Constants.TEST_INDEX;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wujunshen.elasticsearch7.TestConfiguration;
import com.wujunshen.elasticsearch7.common.domain.height.Bottom;
import com.wujunshen.elasticsearch7.common.domain.height.Highest;
import com.wujunshen.elasticsearch7.common.domain.height.Middle;
import com.wujunshen.elasticsearch7.common.domain.height.Upper;
import com.wujunshen.elasticsearch7.common.repository.height.HighestRepository;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.join.ScoreMode;
import org.assertj.core.util.Lists;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class HeightQueryTest {
    private ObjectMapper mapper = new ObjectMapper();
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;
    @Resource
    private HighestRepository highestRepository;
    
    @BeforeEach
    public void init() {
        // 创建index
        if (!elasticsearchTemplate.indexExists(TEST_INDEX)) {
            elasticsearchTemplate.createIndex(TEST_INDEX);
        }
        
        elasticsearchTemplate.putMapping(Highest.class);
        
        elasticsearchTemplate.refresh(Highest.class);
        
        elasticsearchTemplate.putMapping(Upper.class);
        
        elasticsearchTemplate.refresh(Upper.class);
        
        elasticsearchTemplate.putMapping(Middle.class);
        
        elasticsearchTemplate.refresh(Middle.class);
        
        elasticsearchTemplate.putMapping(Bottom.class);
        
        elasticsearchTemplate.refresh(Bottom.class);
    }
    
    @AfterEach
    public void clear() {
    }
    
    /**
     * 嵌套查询, 内嵌文档查询
     */
    @Test
    public void testNestedQuery() throws IOException {
        // 创建数据
        Highest highest =
                Highest.builder()
                        .id(1L)
                        .upper(
                                Upper.builder()
                                        .id(1L)
                                        .middle(
                                                Middle.builder()
                                                        .id(1L)
                                                        .bottom(Bottom.builder().id(1L).intValue(8).stringValue("test").build())
                                                        .build())
                                        .build())
                        .build();
        highestRepository.save(highest);
        
        // 准备查询
        QueryBuilder queryBuilder =
                QueryBuilders.nestedQuery(
                        "upper.middle.bottom",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("upper.middle.bottom.intValue", 8))
                                .must(QueryBuilders.matchQuery("upper.middle.bottom.stringValue", "test")),
                        ScoreMode.None);
        
        Iterable<Highest> iterable = highestRepository.search(queryBuilder);
        
        List<Highest> list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(1));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "middle.bottom",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("middle.bottom.intValue", 8))
                                .must(QueryBuilders.matchQuery("middle.bottom.stringValue", "test")),
                        ScoreMode.None);
        
        iterable = highestRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "bottom",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("bottom.intValue", 8))
                                .must(QueryBuilders.matchQuery("bottom.stringValue", "test")),
                        ScoreMode.None);
        
        iterable = highestRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", mapper.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        highestRepository.delete(highest);
    }
}
