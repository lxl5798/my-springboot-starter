package com.wujunshen.elasticsearch7.common.domain.product;

import static com.wujunshen.elasticsearch7.common.utils.Constants.ES_INDEX_TEST;
import static com.wujunshen.elasticsearch7.common.utils.Constants.ES_TYPE_TEST;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author frank woo(吴峻申) <br> email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(
        indexName = ES_INDEX_TEST,
        type = ES_TYPE_TEST,
        shards = 1,
        replicas = 0,
        refreshInterval = "-1")
public class Spu {
    @Id
    private Long id;
    
    private String productCode;
    
    private String productName;
    
    private String brandCode;
    
    private String brandName;
    
    private String categoryCode;
    
    private String categoryName;
    
    private String imageTag;
    
    @Builder.Default
    private List<Sku> skus = new ArrayList<>();
    
    private String highlightedMessage;
}
