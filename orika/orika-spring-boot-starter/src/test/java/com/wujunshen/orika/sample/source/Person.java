package com.wujunshen.orika.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@AllArgsConstructor
public class Person {
    private String name;
    private String nickname;
}
