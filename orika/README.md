# Orika

#### 项目介绍

参见orika-spring-boot-starter项目

在/resources/META_INF/spring.factories文件中指定自动装配类

OrikaAutoConfiguration是自动装配类。

#### 使用说明

1. 在pom文件中加入starter依赖

    ```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>orika-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    ```

2. 在application.yml文件中,如有必要可加入相关属性，如下：

    ```
    orika:
      map-nulls: false
      stringEnumeration:
        - class-name: com.ueapay.orika.starter.dest.Sex
          convert-name: SexConvert
      zeroEnumeration:
        - class-name: com.ueapay.orika.starter.dest.Leaf
          convert-name: LeafConvert
      enumeration:
        - class-name: com.ueapay.orika.starter.dest.BookType
          convert-name: BookTypeConvert
      json:
        - class-name: com.ueapay.orika.starter.dest.BookInfo
          convert-name: BookInfoConvert
    ```

    这里参见com.wujunshen.orika.properties.OrikaProperties属性配置类，所有属性缺省为true，因此隐式和显式设置为true效果是一样的。
    只有需要显式设置为false时，可在application.yml文件中配置

    设置orika.map-nulls=false后，参见com.wujunshen.orika.test.UnitTest类中givenSrcWithNullAndGlobalConfigForNoNullWhenFailsToMapThenCorrect和
    givenSrcWithNullAndGlobalConfigForNullWhenFailsToMapThenCorrect方法及其注释，并运行查看效果

    另外介绍缺省的4种类型转换器
    参见com.wujunshen.orika.properties.OrikaProperties属性配置类可看见

    ```
    private String valueConvert = "valueConvert";

    private String dateConvert = "dateConvert";
    ```

    这里valueConvert转换器是将类中Object类型属性变量和String类型属性变量进行互相转换，缺省转换器名为"valueConvert"，也可在application.yml文件中设置

    ```
    orika:
       valueConvert: XXX
    ```

    来覆盖替换缺省转换器名

    参见com.wujunshen.orika.test.UnitTest类中givenSrcWithObjectFieldWhenMapsThenCorrect和givenReverseSrcWithObjectFieldWhenMapsThenCorrect方法及其注释，并运行查看效果

    dateConvert转换器是将类中java.util.Date类型属性变量和日期格式为"yyyy-MM-dd HH:mm:ss"的String类型属性变量进行互相转换
    缺省转换器名为"dateConvert"，，也可在application.yml文件中设置

    ```
    orika:
       dateConvert: XXX
    ```

    来覆盖替换缺省转换器名

    参见com.wujunshen.orika.test.UnitTest类中givenSrcWithDateFieldWhenMapsThenCorrect和givenReverseSrcWithDateFieldWhenMapsThenCorrect方法及其注释，并运行查看效果

    另外application.yml文件
    ```
    orika:
      json:
        - class-name: com.wujunshen.orika.sample.dest.BookInfo
          convert-name: BookInfoConvert             
    ```

    是设置jsonConvert转换器，将类中具体实体类型属性变量，使用spring中的Jackson框架转换成JSON字符串类型属性变量，也可互相转换

    由于需要指定具体的实体类型对象，才能进行JSON转换，因此该转换器没有缺省配置，必须显式在application.yml文件中设置。
    
    如上代码，转换器名为"bookInfoConvert"，具体实体类型对象全路径名为com.wujunshen.orika.sample.dest.BookInfo

    还有一个是enumConvert转换器，application.yml文件中内容如下

    ```
    orika:
      enumeration:
          - class-name: com.wujunshen.orika.sample.dest.BookType
            convert-name: BookTypeConvert
    ```

    它是将类中枚举类型属性变量和代表其code的Integer类型属性变量进行互相转换

    和jsonConvert同样理由，需要指定具体的枚举类型对象，因此没有缺省配置，必须显式在application.yml文件中设置。
    
    如上代码，转换器名为"bookTypeConvert"，具体实体类型对象全路径名为com.wujunshen.orika.sample.dest.BookType

    参见com.wujunshen.orika.test.UnitTest类中givenSrcWithJSONAndNumFieldWhenMapsThenCorrect和
    
    givenReverseSrcWithJSONAndNumFieldWhenMapsThenCorrect方法及其注释，并运行查看效果

    注意:jsonConvert和enumConvert都可以配置多个要转换成JSON字符串的实体类和枚举类

    只要按照application.yml文件中内容格式
    ```
    orika:
        json:
             - class-name: XXX
               convert-name: YYY 
        enumeration:
             - class-name: III
               convert-name: JJJ                     
    ```
    这样指定转换器名和实体类，枚举类即可
    
    另外新版又增加了stringEnumeration和zeroEnumeration两种枚举类装换器.
    
    stringEnumeration是用来将类中枚举类型属性变量和代表其code的String类型属性变量进行互相转换
    
    zeroEnumeration是将类中枚举类型属性变量和代表其code,且索引从0开始的的Integer类型属性变量进行互相转换
    
    这两个和enumConvert转换器用法类似,就不具体介绍了,可参考UnitTest类中测试代码

3. 运行test包下UnitTest类中各方法

   如下图

    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/231718_3fc9bc69_43183.jpeg "20200322231706.jpg")

    具体使用和讲解可见[http://www.baeldung.com/orika-mapping](http://www.baeldung.com/orika-mapping)

    这是目前个人认为最全面的Orika使用方法介绍