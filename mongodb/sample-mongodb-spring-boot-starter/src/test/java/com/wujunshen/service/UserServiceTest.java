package com.wujunshen.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import com.wujunshen.mongodb.SampleMongodbSpringBootStarterApplication;
import com.wujunshen.mongodb.entity.User;
import com.wujunshen.mongodb.service.UserService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author frank woo(吴峻申) <br>
 * Date: 2018/7/24 <br>
 * Time: 18:10 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SampleMongodbSpringBootStarterApplication.class)
public class UserServiceTest {
    @Resource
    private UserService userService;
    
    private User masterUser;
    private User slaveUser;
    
    @BeforeEach
    public void setUp() {
        masterUser = User.builder().userName("wujunshen").password("wujunshen").build();
        
        slaveUser = User.builder().userName("wujunshen-slave").password("wujunshen-slave").build();
    }
    
    @AfterEach
    public void tearDown() {
        masterUser = null;
        slaveUser = null;
    }
    
    @Test
    public void getUserOfMaster() {
        assertThat(
                userService.getUserOfMaster("wujunshen", "wujunshen").getUserName(),
                is(masterUser.getUserName()));
        
        assertThat(
                userService.getUserOfMaster("wujunshen", "wujunshen").getPassword(),
                is(masterUser.getPassword()));
    }
    
    @Test
    public void getUserOfSlave() {
        assertThat(
                userService.getUserOfSlave("wujunshen-slave", "wujunshen-slave").getUserName(),
                is(slaveUser.getUserName()));
        
        assertThat(
                userService.getUserOfSlave("wujunshen-slave", "wujunshen-slave").getPassword(),
                is(slaveUser.getPassword()));
    }
    
    @Test
    public void saveOfMaster() {
        if (!userService.existsOfMaster("wujunshen")) {
            User user = userService.saveOfMaster(masterUser);
            assertNotNull(user);
            assertThat(user.getUserName(), equalTo("wujunshen"));
            assertThat(user.getPassword(), equalTo("wujunshen"));
        }
    }
    
    @Test
    public void saveOfSlave() {
        if (!userService.existsOfSlave("wujunshen-slave")) {
            User user = userService.saveOfSlave(slaveUser);
            assertNotNull(user);
            assertThat(user.getUserName(), equalTo("wujunshen-slave"));
            assertThat(user.getPassword(), equalTo("wujunshen-slave"));
        }
    }
    
    @Test
    public void existsOfMaster() {
        assertThat(userService.existsOfMaster("wujunshen"), is(true));
    }
    
    @Test
    public void existsOfSlave() {
        assertThat(userService.existsOfSlave("wujunshen-slave"), is(true));
    }
}
