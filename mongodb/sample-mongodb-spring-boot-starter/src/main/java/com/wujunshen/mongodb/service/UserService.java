package com.wujunshen.mongodb.service;

import com.wujunshen.mongodb.entity.User;

/**
 * @author frank woo(吴峻申) <br>
 * Date:  2018/7/24 <br>
 * Time:  18:02 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public interface UserService {
    /**
     * 主库获取用户数据
     *
     * @param userName 用户名
     * @param password 密码
     * @return 用户对象
     */
    User getUserOfMaster(String userName, String password);
    
    /**
     * 从库获取用户数据
     *
     * @param userName 用户名
     * @param password 密码
     * @return 用户对象
     */
    User getUserOfSlave(String userName, String password);
    
    /**
     * 保存主库用户数据
     *
     * @param user 用户对象
     * @return 用户对象
     */
    User saveOfMaster(User user);
    
    /**
     * 保存从库用户数据
     *
     * @param user 用户对象
     * @return 用户对象
     */
    User saveOfSlave(User user);
    
    /**
     * 查询主库是否存在相应用户数据
     *
     * @param userName 用户名
     * @return 存在为true, 不存在为false
     */
    boolean existsOfMaster(String userName);
    
    /**
     * 查询从库是否存在相应用户数据
     *
     * @param userName 用户名
     * @return 存在为true, 不存在为false
     */
    boolean existsOfSlave(String userName);
}