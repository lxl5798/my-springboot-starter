package com.wujunshen.mongodb.repository;

import com.wujunshen.mongodb.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author frank woo(吴峻申) <br>
 * Date:  2018/7/23 <br>
 * Time:  14:10 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
}