package com.wujunshen.mongodb.entity;

import com.wujunshen.mongodb.annotation.Collection;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@Builder
@Collection(value = "user")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseMongoObject {
    private String userName;
    
    private String password;
}
