package com.wujunshen.mongodb.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import com.wujunshen.mongodb.TestConfiguration;
import com.wujunshen.mongodb.entity.User;
import com.wujunshen.mongodb.page.Page;
import com.wujunshen.mongodb.utils.MongoUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class MongoTest {
    public static final String TEST_COLLECTION_NAME = "user";
    
    @Resource
    @Qualifier("masterMongoTemplate")
    private MongoTemplate masterMongoTemplate;
    
    private MongoUtils mongoUtils;
    
    @BeforeEach
    public void setUp() {
        mongoUtils = new MongoUtils(masterMongoTemplate);
    }
    
    @AfterEach
    public void tearDown() {
        mongoUtils = null;
    }
    
    @Test
    public void testSave() {
        mongoUtils.drop(TEST_COLLECTION_NAME);
        
        // 保存实体对象
        User user = User.builder().userName("wujunshen-object").password("wujunshen-object").build();
        user = mongoUtils.save(user);
        
        log.info(String.valueOf(user));
    }
    
    @Test
    public void testQuery() {
        // 查询出实体列表
        List<User> userList = mongoUtils.findAll(User.class);
        
        assertThat(userList, notNullValue());
        assertThat(userList, hasSize(equalTo(1)));
        
        // 用Filters生成条件查询，查询名字以2结尾的数据
        Pattern queryPattern = Pattern.compile(".*2", Pattern.CASE_INSENSITIVE);
        Query query = Query.query(Criteria.where("password").regex(queryPattern));
        List<User> filterList = mongoUtils.find(query, User.class);
        
        assertThat(filterList, empty());
        
        // 分页查询，查询密码包含object的数据，查询第1页，每页10条
        Page<User> page = new Page<>(10, 1);
        page.setClazz(User.class); // 指定列表中的对象类型
        queryPattern = Pattern.compile(".*object", Pattern.CASE_INSENSITIVE);
        query = Query.query(Criteria.where("password").regex(queryPattern));
        page = mongoUtils.findPage(page, query, User.class);
        
        log.info(page.toString());
        
        assertThat(page.getList(), notNullValue());
        assertThat(page.getList(), hasSize(equalTo(1)));
        assertThat(page.getTotalCount(), equalTo(1L));
        assertThat(page.getPageCount(), equalTo(1));
        assertThat(page.getPageSize(), equalTo(10));
        assertThat(page.getPageNo(), equalTo(1));
    }
    
    @Test
    public void testUpdate() {
        mongoUtils.drop(TEST_COLLECTION_NAME);
        
        // 保存实体对象
        User user = User.builder().userName("wujunshen-object").password("wujunshen-object").build();
        user = mongoUtils.save(user);
        
        log.info(String.valueOf(user));
        
        Update updatedUser = new Update();
        updatedUser.set("userName", "wujunshen-object-update");
        updatedUser.set("password", "wujunshen-object-update");
        Query query = new Query(Criteria.where("_id").is(user.getId()));
        user = mongoUtils.update(query, updatedUser, User.class);
        
        log.info(String.valueOf(user));
        
        assertThat(user, notNullValue());
    }
    
    @Test
    public void testDelete() {
        mongoUtils.drop(TEST_COLLECTION_NAME);
        
        // 保存实体对象
        User user = User.builder().userName("wujunshen-object").password("wujunshen-object").build();
        user = mongoUtils.save(user);
        
        // 根据ID删除
        boolean isSuccess = mongoUtils.deleteById(user.getId(), User.class);
        
        log.info(String.valueOf(isSuccess));
        
        // 删除一条数据
        user.setUserName("wujunshen-object");
        user.setPassword("wujunshen-object");
        user = mongoUtils.save(user);
        
        isSuccess = mongoUtils.deleteOne(user);
        
        log.info(String.valueOf(isSuccess));
        
        // 批量删除
        List<User> del = new ArrayList<>();
        del.add(User.builder().userName("wujunshen-001").password("wujunshen-001").build());
        del.add(User.builder().userName("wujunshen-002").password("wujunshen-002").build());
        mongoUtils.saveAll(del);
        
        Pattern queryPattern = Pattern.compile(".*wujunshen", Pattern.CASE_INSENSITIVE);
        Query query = Query.query(Criteria.where("password").regex(queryPattern));
        isSuccess = mongoUtils.deleteAll(query, User.class);
        
        log.info(String.valueOf(isSuccess));
    }
    
    @Test
    public void testCount() {
        mongoUtils.drop(TEST_COLLECTION_NAME);
        
        // 保存实体对象
        User user = User.builder().userName("wujunshen-object").password("wujunshen-object").build();
        user = mongoUtils.save(user);
        log.info("user is:{}", user);
        
        Pattern queryPattern = Pattern.compile(".*object", Pattern.CASE_INSENSITIVE);
        Query query = Query.query(Criteria.where("password").regex(queryPattern));
        assertThat(mongoUtils.count(query, User.class), is(1L));
    }
}
