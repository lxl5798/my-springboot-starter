package com.wujunshen.mongodb.test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import com.wujunshen.mongodb.TestConfiguration;
import com.wujunshen.mongodb.utils.MongoUtils;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * @author <a href="mailto:frank_wjs@hotmail.com">吴峻申</a><br>
 * @date 2017/9/15 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class MongoConfigTest {
    @Resource
    @Qualifier("masterMongoTemplate")
    private MongoTemplate masterMongoTemplate;
    
    @Resource
    @Qualifier("slaveMongoTemplate")
    private MongoTemplate slaveMongoTemplate;
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testMasterCollectionNames() {
        MongoUtils mongoDAO = new MongoUtils(masterMongoTemplate);
        List<String> collectionNames = mongoDAO.getCollectionNames();
        
        log.info(collectionNames.toString());
        
        assertThat(collectionNames, notNullValue());
        assertThat(collectionNames, hasSize(CoreMatchers.equalTo(1)));
    }
    
    @Test
    public void testSlaveCollectionNames() {
        MongoUtils mongoDAO = new MongoUtils(slaveMongoTemplate);
        List<String> collectionNames = mongoDAO.getCollectionNames();
        
        log.info(collectionNames.toString());
        
        assertThat(collectionNames, notNullValue());
        assertThat(collectionNames, hasSize(CoreMatchers.equalTo(1)));
    }
}
