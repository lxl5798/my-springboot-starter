package com.wujunshen.mongodb.properties;

import java.util.List;
import lombok.Data;
import org.springframework.lang.NonNull;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
public class MongoGroups implements Validator {
    private List<MongoGroup> groups;
    
    @Override
    public boolean supports(@NonNull final Class<?> clazz) {
        return MongoGroups.class.isAssignableFrom(clazz);
    }
    
    @Override
    public void validate(@NonNull final Object target, @NonNull final Errors errors) {
        // nothing to validate
    }
}
