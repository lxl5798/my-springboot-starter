CREATE TABLE
    simple_book
(
    bookName  VARCHAR(255),
    publisher VARCHAR(255)
                  COLLATE utf8_general_ci,
    bookId    INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (bookId)
)
    ENGINE = MyISAM
    DEFAULT CHARSET = utf8;