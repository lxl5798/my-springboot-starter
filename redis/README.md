# Redis

#### 项目介绍

此项目适用于redis集群环境，且覆盖原有springboot官方redis-starter配置，因此如果是单机redis不适用此自定义starter.
(当然就我个人而言,希望大家都是用redis集群环境.
毕竟在互联网行业,分布式系统现在是大势所趋,如果连一个redis都还是单点,那在安全以及性能方面,做出来的系统都实在是太差了~)

参见redis-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定自动装配类

MyRedisConfiguration是自动装配类,包含下列几项功能
1. 手工构建redis客户端jedis的集群配置信息

2. 使用StringRedisSerializer来序列化和反序列化redis的key值和HashKey值

    使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值和HashValue值

    具体代码实现见MyRedisConfiguration类中下列这段
    ```
    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
         RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
     
         redisTemplate.setConnectionFactory(connectionFactory());
         // 使用StringRedisSerializer来序列化和反序列化redis的key值
         redisTemplate.setKeySerializer(new StringRedisSerializer());
         redisTemplate.setValueSerializer(getJackson2JsonRedisSerializer());
         // 使用StringRedisSerializer来序列化和反序列化redis的HashKey值
         redisTemplate.setHashKeySerializer(new StringRedisSerializer());
         redisTemplate.setHashValueSerializer(getJackson2JsonRedisSerializer());
     
         redisTemplate.afterPropertiesSet();
     
         return redisTemplate;
    }               
    ```
   
3. 不支持原有springboot官方redis-starter配置（以spring.redis为前缀的配置）

#### 使用说明

参见sample-redis-spring-boot-starter项目

1. 在pom文件中加入starter依赖
        
    ```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>redis-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    ```

2. 在application.yml配置文件加入下列内容
    ```
    #redis settings
    redis:
      cluster:
        max-redirects: 6   # 获取失败最大重定向次数
        nodes: 127.0.0.1:7001,127.0.0.1:7002,127.0.0.1:7003,127.0.0.1:7004,127.0.0.1:7005,127.0.0.1:7006
        time-out: 6000
        password:
      pool:
        max-active: 1000 #连接池最大的连接数，若使用负值表示没有限制
        max-wait: 10000 #连接池最大阻塞等待时间
        max-idle: 100 #最大空闲连接数
        min-idle: 10 #最小空闲连接数
    ```

    其中nodes,time-out和password必填，其它可填可不填.password如果没有设置,也可以不填

3. 启动sample-redis-spring-boot-starter应用后，浏览器中输入http://localhost:8014/api/books/9
    其中9为自定义数字,可任意输入数字

    执行后可使用本地redis客户端查看数据是否进入六个集群节点中任何一个节点,我这边运行后效果可见下图

    如下图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/201049_5e2e7b44_43183.jpeg "20200322200538.jpg")

4. 另外一种验证方式,就是在redis-spring-boot-starter模块,运行单元测试包com.wujunshen.redis(个人目前使用Junit5)
    
    运行效果如下图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/200949_e5d07940_43183.jpeg "20200322200452.jpg")