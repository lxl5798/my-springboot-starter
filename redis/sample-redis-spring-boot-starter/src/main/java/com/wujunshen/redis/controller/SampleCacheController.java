package com.wujunshen.redis.controller;

import com.wujunshen.redis.entity.Book;
import com.wujunshen.redis.service.SampleCacheService;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/1/31 9:43 下午 <br>
 */
@Slf4j
@RestController
public class SampleCacheController {
    @Resource
    private SampleCacheService sampleCacheService;
    
    /**
     * @param bookId 传入的bookId
     * @return 成功或失败信息，json格式封装
     */
    @GetMapping(value = "/api/books/{bookId:[0-9]*}")
    public Book getBook(@PathVariable("bookId") Integer bookId) {
        return sampleCacheService.getBookById(bookId);
    }
}
