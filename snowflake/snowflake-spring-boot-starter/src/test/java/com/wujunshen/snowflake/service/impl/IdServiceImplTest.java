package com.wujunshen.snowflake.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import com.wujunshen.snowflake.bean.ID;
import com.wujunshen.snowflake.bean.IdMeta;
import com.wujunshen.snowflake.service.IdService;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.ExceptionUtils;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
public class IdServiceImplTest {
    private long id;
    private Set<Long> set;
    private IdService idService;
    
    @BeforeEach
    public void setUp() {
        set = new HashSet<>();
        id = 352608540609069079L;
        idService = new IdServiceImpl(0);
    }
    
    @AfterEach
    public void tearDown() {
        set = null;
        id = 0L;
        idService = null;
    }
    
    @Test
    public void expId() {
        ID actual = idService.expId(id);
        assertThat(actual.getSequence(), equalTo(23L));
        assertThat(actual.getWorker(), equalTo(92L));
        assertThat(actual.getTimeStamp(), equalTo(84068427231L));
    }
    
    @Test
    public void transTime() {
        assertThat(
                idService.transTime(84068427231L).getTime(), equalTo(84068427231L + IdMeta.START_TIME));
    }
    
    @Test
    public void makeId() {
        long actual = idService.makeId(84068427231L, 92L, 23L);
        
        assertThat(actual, equalTo(id));
    }
    
    @Test
    public void genId() {
        for (int j = 0; j < 1024; j++) {
            IdWorkThread idWorkThread = new IdWorkThread(set, new IdServiceImpl(j));
            Thread t = new Thread(idWorkThread);
            t.setDaemon(true);
            t.start();
        }
        
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            log.error("exception message is:{}", ExceptionUtils.readStackTrace(e));
        }
    }
    
    @RepeatedTest(value = 200)
    public void stressTest() {
        log.info("{}", idService.genId());
    }
    
    @AllArgsConstructor
    static class IdWorkThread implements Runnable {
        private Set<Long> set;
        private IdService idService;
        
        @SneakyThrows
        @Override
        public void run() {
            while (true) {
                long id = idService.genId();
                log.info("{}", id);
                if (!set.add(id)) {
                    log.info("duplicate:{}", id);
                }
            }
        }
    }
}
