package com.wujunshen.snowflake.service;

import com.wujunshen.snowflake.bean.ID;
import java.util.Date;
import org.springframework.stereotype.Service;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Service
public interface IdService {
    /**
     * 生成ID（线程安全）
     *
     * @return id
     */
    long genId();
    
    /**
     * 对id进行解析
     *
     * @param id 生成的ID
     * @return 封装的ID类
     */
    ID expId(long id);
    
    /**
     * 对时间戳单独进行解析
     *
     * @param time 时间戳
     * @return 生成的Date时间
     */
    Date transTime(long time);
    
    /**
     * 根据时间戳和序列号生成ID
     *
     * @param timeStamp 时间戳
     * @param sequence  序列号
     * @return 生成的ID
     */
    long makeId(long timeStamp, long sequence);
    
    /**
     * 根据时间戳、机器ID和序列号生成ID
     *
     * @param timeStamp 时间戳
     * @param worker    机器ID
     * @param sequence  序列号
     * @return 生成的ID
     */
    long makeId(long timeStamp, long worker, long sequence);
}
