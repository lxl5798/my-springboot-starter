package com.wujunshen.snowflake.service;

import com.wujunshen.snowflake.bean.ID;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
public interface IdConverter {
    /**
     * ID和代表的long值转换
     *
     * @param id ID类
     * @return 代表的long值
     */
    long convert(ID id);
    
    /**
     * ID和代表的long值转换
     *
     * @param id long值
     * @return ID类
     */
    ID convert(long id);
}
