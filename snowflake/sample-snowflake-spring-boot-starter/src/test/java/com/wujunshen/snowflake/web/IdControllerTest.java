package com.wujunshen.snowflake.web;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wujunshen.snowflake.bean.ID;
import com.wujunshen.snowflake.bean.MakeID;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles(value = "test")
@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IdControllerTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    
    @Autowired
    private TestRestTemplate template;
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void genId() {
        long actual = template.getForObject("/id", long.class, new HashMap<>());
        
        log.info("actual is: {}", actual);
    }
    
    @Test
    public void explainId() {
        Map<String, String> multiValueMap = new HashMap<>();
        multiValueMap.put("id", "352834416118059008"); // 传值，但要在url上配置相应的参数
        
        ID actual = template.getForObject("/id/{id}", ID.class, multiValueMap);
        
        assertThat(actual.getWorker(), equalTo(1021L));
        assertThat(actual.getSequence(), equalTo(0L));
        assertThat(actual.getTimeStamp(), equalTo(84122280148L));
    }
    
    @Test
    public void transTime() {
        Map<String, String> multiValueMap = new HashMap<>();
        multiValueMap.put("time", "84122280148"); // 传值，但要在url上配置相应的参数
        
        String actual = template.getForObject("/time/{time}", String.class, multiValueMap);
        
        assertThat(actual, equalTo("2017-08-31 15:18:00"));
    }
    
    @Test
    public void makeId() throws Exception {
        String requestBody =
                "{\n"
                        + "  \"worker\": 1021,\n"
                        + "  \"timeStamp\": 84122280148,\n"
                        + "  \"sequence\": 0\n"
                        + "}";
        
        long actual =
                template.postForObject(
                        "/id", OBJECT_MAPPER.readValue(requestBody, MakeID.class), long.class);
        
        log.info("actual is:{}", actual);
        assertThat(actual, equalTo(352834416118059008L));
    }
}
